package com.titans.app;

import android.content.Context;

import java.util.ArrayList;

public class Person {

    private String name, designation, floor, type, status;

    public Person(String name, String designation, String floor, String type, String status) {
        this.name = name;
        this.designation = designation;
        this.floor = floor;
        this.type = type;
        this.status = status;
    }


    public String getName() {
        return name;
    }

    public String getFloor() {
        return floor;
    }

    public String getType() {
        return type;
    }

    public String getDesignation() {
        return designation;
    }

    public String getStatus() {
        return status;
    }

    public static ArrayList<Person> AddPersons(Context context) {

        String[] names = context.getResources().getStringArray(R.array.name);
        String[] designations = context.getResources().getStringArray(R.array.designation);
        String[] floor = context.getResources().getStringArray(R.array.floor);
        String[] type = context.getResources().getStringArray(R.array.type);
        String[] status = context.getResources().getStringArray(R.array.status);

        ArrayList<Person> persons = new ArrayList<>();

        for (int i = 0; i < 5; i++) {
            persons.add(new Person(
                    names[i],
                    designations[i],
                    floor[i],
                    type[i],
                    status[i]
            ));
        }
        return persons;
    }

}
