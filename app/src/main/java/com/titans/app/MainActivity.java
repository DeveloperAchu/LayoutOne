package com.titans.app;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        ArrayList<Person> person;

        person = Person.AddPersons(this);

        PersonAdapter adapter = new PersonAdapter(this, person);
        ListView listView = (ListView) findViewById(R.id.list_items);
        listView.setAdapter(adapter);
    }
}
