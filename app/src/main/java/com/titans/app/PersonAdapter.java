package com.titans.app;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v4.content.ContextCompat;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.style.StyleSpan;
import android.text.style.UnderlineSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.PopupMenu;
import android.widget.TextView;

import java.util.List;

public class PersonAdapter extends ArrayAdapter<Person> {


    public PersonAdapter(Context context, List<Person> objects) {
        super(context, 0, objects);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Person person = getItem(position);
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.list_item, parent, false);
        }
        TextView name = (TextView) convertView.findViewById(R.id.name);
        TextView floor = (TextView) convertView.findViewById(R.id.floor);
        TextView type = (TextView) convertView.findViewById(R.id.type);
        TextView status = (TextView) convertView.findViewById(R.id.status);

        ImageButton dropButton = (ImageButton) convertView.findViewById(R.id.button_drop_down);
        dropButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PopupMenu popupMenu = new PopupMenu(getContext(), v);
                popupMenu.inflate(R.menu.popup);
                popupMenu.show();
            }
        });

        String personName = person.getName();
        int len = personName.length();
        String str = "By " + personName + ", " + person.getDesignation();
        SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder(str);

        spannableStringBuilder.setSpan(
                new UnderlineSpan(),
                3,
                3 + len,
                Spanned.SPAN_INCLUSIVE_INCLUSIVE
        );

        spannableStringBuilder.setSpan(
                new StyleSpan(Typeface.BOLD),
                3,
                3 + len,
                Spanned.SPAN_INCLUSIVE_INCLUSIVE
        );

        name.setText(spannableStringBuilder);
        floor.setText(person.getFloor());
        String statusStored = person.getStatus();
        String typeStored = person.getType();

        status.setText(person.getStatus());
        floor.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.greenColor));
        type.setText(person.getType());

        if (statusStored.equals("Config") && typeStored.equals("Corridor"))
            status.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.cardColor));

        if (statusStored.equals("Config") && typeStored.equals("Common Room"))
            status.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.orangeColor));

        if (statusStored.equals("Occupied"))
            status.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.limeColor));

        if (statusStored.equals("Unoccupied")) {
            status.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.redColor));
            name.setText("");
        }

        return convertView;
    }
}

